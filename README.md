# CHALLENGE 5 BINAR FSW

### Faradini Usha (FSW-14)

## ERD Database 
![ERD](/public/images/ERD_binar.png)

## Link untuk masing-masing halaman
```http
Halaman list car (index) = http://localhost:8000
Halaman add new car      = http://localhost:8000/create
Halaman update           = http://localhost:8000/update/:id
```

## Link REST API
```http
GET all car data         = http://localhost:5000/api/cars
GET car data by ID       = http://localhost:5000/api/cars/:id
PUT                      = http://localhost:5000/api/cars
POST                     = http://localhost:5000/api/cars/:id
DELETE                   = http://localhost:5000/api/cars/:id
```

## Contoh Request and Response 

### GET car data by ID 7
#### Request :
```http
http://localhost:5000/api/cars/7
```
#### Response :
```javascript
[
    {
    "id": 7,
    "name": "Chevrolet",
    "type": "Family Car",
    "price": "500000",
    "image": "1650612251608.jpg",
    "size": "Medium",
    "createdAt": "2022-04-22T07:24:11.634Z",
    "updatedAt": "2022-04-22T07:24:11.637Z"
}
]
```

