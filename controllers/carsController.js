const Cars = require("../models").Cars;
const { Op } = require("sequelize");

const list = async (req, res) => {
  if (req.query.q && req.query.q !== "") {
    return await Cars.findAll({
      where: {
        name: {
          [Op.iLike]: `%${req.query.q}%`,
        },
      },
    }).then((car) => res.send(car));
  }
  return await Cars.findAll({
    order: [["id", "ASC"]],
  }).then((cars) => res.send(cars));
};

const getDataById = async (req, res) => {
  return await Cars.findByPk(req.params.id)
    .then((car) => {
      if (!car) {
        return res.status(404).send({
          message: `Data with id ${req.params.id} is not found`,
        });
      }
      return res.status(200).send(car);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
};

const add = async (req, res) => {
  return await Cars.create({
    name: req.body.name,
    type: req.body.type,
    price: req.body.price,
    image: req.body.image,
    size: req.body.size,
    createdAt: new Date(),
    updatedAt: new Date(),
  })
    .then((car) => res.status(200).send(car))
    .catch((error) => res.status(400).send(error));
};

const update = async (req, res) => {
  return await Cars.findByPk(req.params.id)
    .then((car) => {
      if (!car) {
        return res.status(404).send({
          message: `Data with id ${req.params.id} is not found`,
        });
      }
      return car
        .update({
          name: req.body.name,
          type: req.body.type,
          price: req.body.price,
          image: req.body.image,
          size: req.body.size,
          createdAt: new Date(),
          updatedAt: new Date(),
        })
        .then(() => res.status(200).send(car))
        .catch((error) => res.status(404).send(error));
    })
    .catch((error) => res.status(400).send(error));
};

const deleteData = async (req, res) => {
  return await Cars.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => res.status(204).send())
    .catch((error) => res.status(400).send(error));
};

module.exports = { list, getDataById, add, update, deleteData };
