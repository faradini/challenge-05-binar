"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("Cars", [
      {
        name: "Mazda",
        type: "Family Car",
        price: "450000",
        image: "mazdacar.png",
        size: "Medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Cherry",
        type: "SUV",
        price: "800000",
        image: "cherrycar.jpg",
        size: "Large",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Brio",
        type: "City Car",
        price: "300000",
        image: "brio.png",
        size: "Small",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Cars", null, {});
  },
};